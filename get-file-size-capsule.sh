#!/bin/bash
input_size=$(echo $1 | sed 's/M//')
# echo $(($input_size+1))
#dd if=/dev/zero of=output.dat  bs=5M  count=1

for file in ./*
do
    file_size=$(ls -l --block-size=M $file | awk '{print $5}' | sed 's/M//')

    if [ $(($file_size)) -ge $(($input_size)) ];then
        ls -l --block-size=M $file
        exit 1
    fi
done